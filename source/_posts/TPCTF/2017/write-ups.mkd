---
layout: post
title: "Takoma Park CTF 2017 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - web
  - misc
date: 2017/12/04
thumbnail: /images/ctf.png
authorId: noraj
---

## Information

### Version

| By    | Version | Comment
| ---   | ---     | ---
| noraj | 1.0     | Creation

### CTF

- **Name** : Takoma Park CTF 2017
- **Website** : [tpctf.com](https://tpctf.com/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/535)


## 20 - Ads - Web

> I hid a flag somewhere on this site. Find it.
>
> Author: Clarence Lam

At the end of the [about page](https://tpctf.com/about) you can see the flag: `tpctf{thx_4_n0_adb1ock}` (without adblocker or by reading source code).

## 5 - Bad Restaurant - Crypto

> I asked for a salad, but I got this instead: jfsjv{qbb_heqti_buqt_je_hecu}.
>
> Author: Kevin Higgs

Caesar cipher, ROT16: `tpctf{all_roads_lead_to_rome}`.

## 60 - What is this place? - Web

> krnh11 made a program, but now he's kinda lost.
> https://repl.it/IiUz/
>
> Author: Clarence Lam


https://repl.it/IiUz/ is the short link for https://repl.it/@clarencelam/caesar-again,.

The page displays this message:

```
what is my location? i think i hid a hex string there
i think i hid a flag at http://tinyurl.com/1st-16-chars-of-the-hex
put text in text box and hit submit
```

Entering the `document.location.href` shows me `https://replit.org/data/web_project/64df16458ba95e01f7f67706af4602ed/index.html`, as the challenge suggested _what is my location?_. There must be something to do with `64df16458ba95e01f7f67706af4602ed`.

For the first part of the flag, the link http://tinyurl.com/1st-16-chars-of-the-hex redirects to https://hexed.it/#base64:Zmlyc3QgcGFydCBvZiB0aGUgZmxhZyBpcyB0cGN0Znt3NGk3Xw== showing `first part of the flag is tpctf{w4i7_`.

Let's find the other part with `64df16458ba95e01f7f67706af4602ed`. Displaying the hex as ascii gives nothing, it looks like a MD5 hash so I tried to break it without success. As the web page is about caesar bruteforce I also tried to break the 25 others variations of the hash.

Wait a minute, the link with the first part of the flag was `http://tinyurl.com/1st-16-chars-of-the-hex`. Maybe it's an hint.

```
irb(main):005:0> "64df16458ba95e01f7f67706af4602ed"[0..15]
=> "64df16458ba95e01"
```

What to do with that? Flag is not `tpctf{w4i7_64df16458ba95e01}`, `64df16458ba95e01` doesn't look like leet speak, it can be a CRC-64 but I don't think it is.

Wait one more minute, http://tinyurl.com/1st-16-chars-of-the-hex, let's replace it with the 1st 16 chars.

http://tinyurl.com/64df16458ba95e01 redirects to https://hexed.it/#hex:7365636f6e642070617274206f662074686520666c6167206973207233706c5f643033735f376831353f7d

```
$ printf %s '7365636f6e642070617274206f662074686520666c6167206973207233706c5f643033735f376831353f7d' | xxd -r -p
second part of the flag is r3pl_d03s_7h15?}
```

So the flag is `tpctf{w4i7_r3pl_d03s_7h15?}`.

Note: I hate that kind of useless guessing challenge with no security in it.

## 60 - Management - Web

> I've created a [new website](http://web.tpctf.com:8558/) so you can do all your important management. It includes users, creating things, reading things, and... well, not much else. Maybe there's a flag?
>
Second instance running at 52.90.229.46:8558
>
>tpctf{san1t1z3_y0ur_1npu7s} is not the correct flag. Look harder ;)
>
> Note: the flag format is flag{}, not the usual tpctf{}
>
> Author: Kevin Higgs


A normal SQL query seems to be

    SELECT `1` FROM users WHERE name = 'custom-kevin';

and displays the result.

A normal HTTP request seems to be:

```
POST / HTTP/1.1
Host: 52.90.229.46:8558
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://52.90.229.46:8558/
Content-Type: application/x-www-form-urlencoded
Content-Length: 27
Cookie: user=clarence
Connection: close
Upgrade-Insecure-Requests: 1

number=1&value=&action=Read
```

So what if we try to display usernames by injecting a payload in the `number` parameter? Let's try this:

    name` FROM users LIMIT 1 OFFSET 0-- -

as a result we get `custom-Hi` so it's working. Now let's try to increment:

    name` FROM users LIMIT 1 OFFSET 1-- -

 bingo we get the flag: `flag{aLW4ys_ESC4PE_3v3rYTH1NG!!!!!}`.

## 10 - That's not latin... - Misc

> Free 10 points...right? Here's the flag, I hope:
>
> `tpctf{nеv3r_7h15_3z}`
>
> Author: Clarence Lam

They may want us to translate this from leet to english: `tpctf{nеver_this_ez}`, that doesn't work, `tpctf{nеver_this_easy}` either.

That's not in latin either: `tpctf{numquam_facilis}`, `tpctf{numqu4m_f4c1l15}`.

But wy reading [this](https://fr.wikipedia.org/wiki/Leet_speak#Remarque) I understand that's maybe not leet but it can be [arabizi](https://fr.wikipedia.org/wiki/Alphabet_de_tchat_arabe). But that's not that either.

Guessing challenges are always a pain...

Latin numbers doesn't work `tpctf{nеvIIIr_VIIhIV_IIIz}`.

Correcting the missing leet doesn't work `tpctf{n3v3r_7h15_3z}`.

I tried `tpctf{nеver_this_easy}` but not `tpctf{n3v3r_7h15_345y}`, but that's not that.

Removing numbers `tpctf{nеvr_h_z}`...

They may use some character tricks: `tpctf{nеv3r_7h15_3z}` is not `tpctf{nev3r_7h15_3z}`, preventing from copy/pasting. See [IDN homograph attack](https://en.wikipedia.org/wiki/IDN_homograph_attack).

A serious subject when talking about DNS typosquatting but here just a useless guessing challenge.

## 50 - Methods - Web

> Make the server give you the flag. Navigate to the problem page from [here](http://methods.tpctf.com/).
>
> Author: Steven Su

Try the differents HTTP methods, `OPTIONS` to see all available methods and then...

```
$ curl -v -X OPTIONS http://methods.tpctf.com/problem
*   Trying 216.239.32.21...
* TCP_NODELAY set
* Connected to methods.tpctf.com (216.239.32.21) port 80 (#0)
> OPTIONS /problem HTTP/1.1
> Host: methods.tpctf.com
> User-Agent: curl/7.56.1
> Accept: */*
>
< HTTP/1.1 200 OK
< Content-Type: text/html; charset=utf-8
< Allow: OPTIONS, HEAD, DELETE, POST, GET
< Set-Cookie: session=eyJfcGVybWFuZW50Ijp0cnVlfQ.DQWYlg.AP3EbqS68v2AWoE7qPBxuj-5rfI; Expires=Sun, 03-Dec-2017 14:35:50 GMT; HttpOnly; Path=/
< X-Cloud-Trace-Context: b73375bee7db692185b6dd624ba02a3f
< Date: Sun, 03 Dec 2017 14:15:50 GMT
< Server: Google Frontend
< Content-Length: 0
< Expires: Sun, 03 Dec 2017 14:15:50 GMT
< Cache-Control: private
<
* Connection #0 to host methods.tpctf.com left intact

$ curl -v -X DELETE http://methods.tpctf.com/problem
*   Trying 216.239.34.21...
* TCP_NODELAY set
* Connected to methods.tpctf.com (216.239.34.21) port 80 (#0)
> DELETE /problem HTTP/1.1
> Host: methods.tpctf.com
> User-Agent: curl/7.56.1
> Accept: */*
>
< HTTP/1.1 200 OK
< Content-Type: text/html; charset=utf-8
< Set-Cookie: session=eyJfcGVybWFuZW50Ijp0cnVlfQ.DQWaPA.Xk9LrL0JCcYytopoPk3-VCWGo8s; Expires=Sun, 03-Dec-2017 14:42:52 GMT; HttpOnly; Path=/
< X-Cloud-Trace-Context: a55967d2441e5de7054f7c098f5f5e18
< Date: Sun, 03 Dec 2017 14:22:52 GMT
< Server: Google Frontend
< Content-Length: 58
< Expires: Sun, 03 Dec 2017 14:22:52 GMT
< Cache-Control: private
<
* Connection #0 to host methods.tpctf.com left intact
Flag: tpctf{so_post_and_get_are_not_the_only_http_methods}
```

## 50 - IRC - Misc

> The bot flagbotbot on #tpctf-chal has a flag, but has only a 3% chance of giving it out. The other 97% of the time, it gives out flegs. Flegs look like flags. Don't get fooled by flegs. You can get flagbotbot talking by waiting 30 seconds.
>
> Hint: There isn't a good way to solve this other than staring at the chat box for a while. If you don't want to do that, get someone else to do it. Or something else, for that matter.
>
> Author: Clarence Lam

We just have to wait for the flag, but an IRC bot will do that for us.

`the flag is TPCTF{1|I!1!!|l1|1} that's a fleg btw` is a wrong flag example.

The IRC bot will be using cinch in Ruby:

```ruby
require 'cinch'

bot = Cinch::Bot.new do
    configure do |c|
        c.server   = "irc.freenode.net"
        c.nick     = "noraj_bot"
        c.channels = ["#tpctf-chal"]
    end

    on :channel do |m|
        if m.user.nick == 'flagbotbot'
            regex_flag = /the flag is (TPCTF{.*}) that's a (fleg|flag) btw/
            if regex_flag.match?(m.message)
                flag, type = m.message.match(regex_flag).captures
                if type == 'fleg'
                    puts "- Fleg: #{flag}"
                else
                    puts "++++++++++++++++++++++++++++++++"
                    puts "+++++ Flag: #{flag}"
                    puts "++++++++++++++++++++++++++++++++"
                end
            end
        end
    end
end

bot.start
```
So we can see this in our logs:

```
++++++++++++++++++++++++++++++++
+++++ Flag: TPCTF{1lI|1Il|1I1l}
++++++++++++++++++++++++++++++++
```

## 60 - Secret Doc - Misc

> you found this strange string on the floor
> """1rMrhv9mEAWBU99vE0QIIBN6qavpGvkczWDsiMtJRf3Y"""
>
> Author: Clarence Lam

The string seems to be part of a google doc URL:

https://docs.google.com/document/d/1rMrhv9mEAWBU99vE0QIIBN6qavpGvkczWDsiMtJRf3Y/edit

`CTRL+A` to select all the text:

```
lol no flag here

I tried to write some code but my keyboard is broken

//four ops
var add = function(a,b){return a+b;};
 ar sub = function(a,b){return a-b;};
var mul = function(a,b){return a*b;};
var div = function(a,b){return a/b;};
function str(a){return a toString();};
REGEXMATCH=(g,h)=> .test(h);
function mo (a,b){return a%b} / who needs semico ons
console.log(String.fromCharCode(+REGEXMAT H(/abc/,"qabcd")*79)) // THE QUICK BROWN FOX JU PS OVER THE LAZY DOGS
console.log(String.fromCharCode(+REGEXMATCH(/abc/,"abcd")*mul(6,8))) // the quick brown fox  umps over the lazy dogs
```

Let's try to complete the missing chars:

```
var add = function(a,b){return a+b;};
var sub = function(a,b){return a-b;};
var mul = function(a,b){return a*b;};
var div = function(a,b){return a/b;};
function str(a){return a.toString();};
REGEXMATCH=(g,h)=>g.test(h);
function mod(a,b){return a%b}; // who needs semicolons
String.fromCharCode(+REGEXMATCH(/abc/,"qabcd")*79); // THE QUICK BROWN FOX JUMPS OVER THE LAZY DOGS
String.fromCharCode(+REGEXMATCH(/abc/,"abcd")*mul(6,8)); // the quick brown fox jumps over the lazy dogs
```

The Javascript is dsplaying `O` and `0`, that's pretty much useless.

Thank to my mates that thought about the following part.

But maybe writing down the missing chars will help: `v.gd/lCOM0j`.

That looks like an URL. This shorten link redirect to https://hexed.it/#base64+lzjb:AIlQTkcNChoKAAAAAA1JSERSAAAABIAAAAKIhAgCABKHSRZaAAcAAXNSR0IArs4EHOkADQRnQU1BAAAAsY8L/GEFAQAQCXBIWXMAAAQOwwQEAcdvqGQAAAAKD0lEQVQAeF7t28F24jAADAVQZub/v5kAmsioqpNAUigAHeDehZFkx1kYvwMcAECIA97TnwCzqGM4mM/XTgA2udWKoW7qPADWUOswnfrUpwEAQM+rZpuNOWcACkQnvS/6xrQAFWtYrOuxeiAAzCe7DG8BAAAA+CF/++c2LaIA1JSSbRaD4/EAmGueiSK3cpgAa5hPmmxbEaIAbYb2Rw0vetgAewEAgKe2L4AAtbwUwWnNxigAUi+Juq5hcTIAtL2a3lvbuTsAZqS4Ki/MdpggLIpjTe8AWLbLACxxNVTUkxcOAMdu0/tJbaOuAGsa2qptpT46AOvTsz5dl2eiQBjaWqy5egCieABJ//rnbbYkigD7po52W14YdQAT7aK2G1+RXQA4lmcGwyPZRoBRd+eTRVcPACsAL2nfTxDXtNwAshhdqqsHdpkAv7FOWsKpYhgAhqc26hfdEJwEbnwBNngB9wlgAL+rRq8Qk9SnAJMIQlXfuGjxAJ5endXJdPGXALZXAADAG3viAAC2K9VkfIooAJViGKKdotOnANgaxHx4vBkmAPXx08vK7trNAQBtrDqltEnvS0C4qkWobdQAEHwAX+SrpvdnOZkCbwDg7+AV/gP2QH8afnAodAAP/AACYQwAAN6TbwLAACkeRAB7NF/+fwAXAA38A/xC/EL8QvxC//xC/EL8QvxC/EL8QvxC/EL//EL8QvxC/EL8QvxC/EL8Qv/8QvxC/EL8QvxC/EL8QvxCA/xC/EIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAK/vcPgAOg4AjnxzEl0bAAAAAABJRU5ErkIAYII=

Then we save the PNG image and open it with Stegsolve. On the blue or red plane you can see the flag: `tpctf{fl4g5_r_4_n00bs}`.

Another guessing challenge not talking about security.

## 40 - Woah! - Misc

> lel i haz good meme
>
> Author: Kevin Higgs

On the PNG there are several hidden numbers:

+ Trailing data: `c=23273044714507614583919107642780731232409437015529456693716961465133831310123918470882082327335507219289041144186168765420579209702030715438341449893348841695639896994241125`
+ Green layer: `e=3`
+ LSB: `n=117528486827789400165355036597757499772267701336144528618362083257361513763806238831790113655715882881718725332491830831528846581852724863384001329225122149955762187282660943708267841791772849325196822992527135907390322474087022656372336737574135803454741012331637124409253565628911728336882317184586299251643`

It looks like breaking weak RSA so the crypto guy of my team broke it and got the flag: `tpctf{rs4_and_st3g0_w0w}`.
