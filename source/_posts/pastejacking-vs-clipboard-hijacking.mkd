---
layout: post
title: "Pastejacking vs clipboard hijacking"
lang: en
categories:
  - security
  - news
tags:
  - security
  - news
  - javascript
  - pastejacking
  - hijacking
date: 2016/06/02
thumbnail: /images/security-265130_640.jpg
authorId: noraj
---
## Clipboard hijacking

It as been possible for a long time to use css (-9999px position trick) to append malicious content to the clipboard. Users doesn't notice anything and may execute unwanted commands.

This type of attack is called clipboard hijacking and is dangerous when the user copies something from the web inside his terminal.

## Pastejacking

Dylan Ayrey, security researcher, published a variation of this attack that use only JavaScript.
Difference with clipboard hijacking, is that pastejacking can copy text after an event.
With clipboard hijacking user has to copy the full text but with pastejacking one character is enough.

## Versus

Pastejacking is more powerful but is easier to block : user has just to block JavaScript in his browser or use a plugin such as [NoScript][noscript].

Clipboard hijacking is less threatening but harder to block, user will need an add-on for his terminal, for example, that blocks copied text containing line feeds.

[noscript]:https://noscript.net/ "NoScript"
