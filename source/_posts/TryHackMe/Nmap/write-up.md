---
layout: post
title: "Nmap - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - recon
  - network
  - thm
  - nmap
date: 2020/09/03
thumbnail: /images/TryHackMe/Nmap.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Nmap
- **Profile:** [tryhackme.com](https://tryhackme.com/room/rpnmap)
- **Difficulty:** Easy
- **Description**: Part of the Red Primer series, intro to scanning

![Nmap](/images/TryHackMe/Nmap.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
pikaur -S nmap
```

## [Task 2] Nmap Quiz

### #1

> First, how do you access the help menu?

{% spoiler -h %}

### #2

> Often referred to as a stealth scan, what is the first switch listed for a 'Syn Scan'?

{% spoiler -sS %}

### #3

> Not quite as useful but how about a 'UDP Scan'?

{% spoiler -sU %}

### #4

> What about operating system detection?

{% spoiler -O %}

### #5

> How about service version detection?

{% spoiler -sV %}

### #6

> Most people like to see some output to know that their scan is actually doing things, what is the verbosity flag?

{% spoiler -v %}

### #7

> What about 'very verbose'? (A personal favorite)

{% spoiler -vv %}

### #8

> Sometimes saving output in a common document format can be really handy for reporting, how do we save output in xml format?

{% spoiler -oX %}

### #9

> Aggressive scans can be nice when other scans just aren't getting the output that you want and you really don't care how 'loud' you are, what is the switch for enabling this?

{% spoiler -A %}

### #10

> How do I set the timing to the max level, sometimes called 'Insane'?

{% spoiler -T5 %}

### #11

> What about if I want to scan a specific port?

{% spoiler -p %}

### #12

> How about if I want to scan every port?

{% spoiler -p- %}

### #13

> What if I want to enable using a script from the nmap scripting engine? For this, just include the first part of the switch without the specification of what script to run.

{% spoiler --script %}

### #14

> What if I want to run all scripts out of the vulnerability category?

{% spoiler --script vuln %}

### #15

> What switch should I include if I don't want to ping the host?

{% spoiler -Pn %}

## [Task 3] Nmap Scanning


### #1

> Let's go ahead and start with the basics and perform a syn scan on the box provided. What will this command be without the host IP address?

{% spoiler nmap -sS %}

### #2

> After scanning this, how many ports do we find open under 1000?

{% spoiler 2 %}

### #3

> What communication protocol is given for these ports following the port number?

{% spoiler tcp %}

### #4

> Perform a service version detection scan, what is the version of the software running on port 22?

{% spoiler 6.6.1p1 %}

```
$ sudo nmap -sV 10.10.8.168
Starting Nmap 7.80 ( https://nmap.org ) at 2020-09-03 18:58 CEST
Nmap scan report for 10.10.8.168
Host is up (0.031s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.10 (Ubuntu Linux; protocol 2.0)
80/tcp open  http    Apache httpd 2.4.7 ((Ubuntu))
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 8.16 seconds
```

### #5

> Perform an aggressive scan, what flag isn't set under the results for port 80?

{% spoiler httponly %}

```
p sudo nmap -A 10.10.8.168
Starting Nmap 7.80 ( https://nmap.org ) at 2020-09-03 18:59 CEST
Nmap scan report for 10.10.8.168
Host is up (0.029s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   1024 c8:20:80:a7:b0:35:25:b0:89:77:a3:b3:78:0e:fc:c7 (DSA)
|   2048 b5:9a:f6:41:6d:58:08:8e:4e:61:73:1e:bc:86:e9:3f (RSA)
|   256 66:3d:93:16:12:b0:61:e4:d6:6e:79:2f:c0:01:af:8f (ECDSA)
|_  256 21:db:ed:2a:03:94:3d:3e:4c:af:3e:4c:a8:c7:db:58 (ED25519)
80/tcp open  http    Apache httpd 2.4.7 ((Ubuntu))
| http-cookie-flags:
|   /:
|     PHPSESSID:
|_      httponly flag not set
| http-robots.txt: 1 disallowed entry
|_/
|_http-server-header: Apache/2.4.7 (Ubuntu)
| http-title: Login :: Damn Vulnerable Web Application (DVWA) v1.10 *Develop...
|_Requested resource was login.php
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.80%E=4%D=9/3%OT=22%CT=1%CU=36679%PV=Y%DS=2%DC=T%G=Y%TM=5F5120F7
OS:%P=x86_64-unknown-linux-gnu)SEQ(SP=FB%GCD=1%ISR=10B%TI=Z%CI=I%II=I%TS=8)
OS:OPS(O1=M508ST11NW6%O2=M508ST11NW6%O3=M508NNT11NW6%O4=M508ST11NW6%O5=M508
OS:ST11NW6%O6=M508ST11)WIN(W1=68DF%W2=68DF%W3=68DF%W4=68DF%W5=68DF%W6=68DF)
OS:ECN(R=Y%DF=Y%T=40%W=6903%O=M508NNSNW6%CC=Y%Q=)T1(R=Y%DF=Y%T=40%S=O%A=S+%
OS:F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%RD=0%Q=)T
OS:5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=40%W=0%S=A%A=
OS:Z%F=R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U1(R=Y%DF
OS:=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=40
OS:%CD=S)

Network Distance: 2 hops
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 143/tcp)
HOP RTT      ADDRESS
1   28.32 ms 10.9.0.1
2   28.59 ms 10.10.8.168

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 21.87 seconds
```

### #6

> Perform a script scan of vulnerabilities associated with this box, what denial of service (DOS) attack is this box susceptible to? Answer with the name for the vulnerability that is given as the section title in the scan output. A vuln scan can take a while to complete. In case you get stuck, the answer for this question has been provided in the hint, however, it's good to still run this scan and get used to using it as it can be invaluable.

{% spoiler http-slowloris-check %}
