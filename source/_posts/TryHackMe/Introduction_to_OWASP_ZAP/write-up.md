---
layout: post
title: "Introduction to OWASP ZAP - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - network
  - thm
  - zaproxy
date: 2020/09/10 22:23:00
thumbnail: /images/TryHackMe/Introduction_to_OWASP_ZAP.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Introduction to OWASP ZAP
- **Profile:** [tryhackme.com](https://tryhackme.com/room/learnowaspzap)
- **Difficulty:** Easy
- **Description**: Learn how to use OWASP ZAP from the ground up. An alternative to BurpSuite.

![Introduction to OWASP ZAP](/images/TryHackMe/Introduction_to_OWASP_ZAP.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
pikaur -S	zaproxy
```

## [Task 1] Intro to ZAP

> What does ZAP stand for?

Answer: {% spoiler Zed Attack Proxy %}

## [Task 5] Manual Scanning

> What IP do we use for the proxy?

Answer: {% spoiler 127.0.0.1 %}

## [Task 8] Bruteforce Web Login 

> Use ZAP to bruteforce the DVWA 'brute-force' page. What's the password?

Answer: {% spoiler password %}
