---
layout: post
title: "tmux - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - tmux
  - linux
date: 2020/09/10 22:31:00
thumbnail: /images/TryHackMe/tmux.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** tmux
- **Profile:** [tryhackme.com](https://tryhackme.com/room/rptmux)
- **Difficulty:** Easy
- **Description**: Part of the Red Primer series, learn to use tmux!

![tmux](/images/TryHackMe/tmux.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
pikaur -S	tmux
```

## [Task 1] Screens wishes it was this cool.

**#2**

> Once tmux is installed, let's launch a new session. What command do we use to launch a new session without a custom name?

Answer: {% spoiler %}

**#3**

> All tmux commands start with a keyboard button combination. What is the first key in this combination?

Answer: {% spoiler control %}

**#4**

> How about the second key? Note, these keys must be pressed at the same time and released before pressing the next target key in the combination.

Answer: {% spoiler b %}

**#5**

> Lets go ahead and detach from our newly created tmux session. What key do we need to add to the combo in order to detach?

Answer: {% spoiler d %}

**#6**

> Well shoot, we've detached from our session. How do we list all of our sessions?

Answer: {% spoiler tmux ls %}

Also works with:

```
$ tmux list-sessions
0: 2 windows (created Wed Sep  9 14:50:21 2020) (attached)
```

**#7**

> What did our session name default to when we created one without a set name?

Answer: {% spoiler 0 %}

**#8**

> Now that we've found the name of our session, how do we attach to it?

Answer: {% spoiler tmux a -t 0 %}

Also works with:

```
$ tmux attach-session -t 0
```

**#9**

> Let's go ahead and make a new window in this session. What key do we add to the combo in order to do this?

Answer: {% spoiler c %}

**#12**

> Whew! Plenty of output to work with now! If you work with a relatively small terminal like me, this output might not all fit on screen at once. To fix that, let's enter 'copy mode'. What key do we add to the combo to enter copy mode?

Answer: {% spoiler [ %}

**#13**

> Copy mode is very similar to 'less' and allows up to scroll up and down using the arrow keys. What if we want to go up to the very top?

Answer: {% spoiler g %}

**#14**

> How about the bottom?

Answer: {% spoiler G%}

**#15**

> What key do we press to exit 'copy mode'?

Answer: {% spoiler q %}

**#16**

> This window we're working in is nice and all but I think we need an upgrade. What key do we add to the combo to split the window vertically?

Answer: {% spoiler `%` %}

**#17**

> How about horizontally?

Answer: {% spoiler `"` %}

**#21**

> Say one of these newly minted panes becomes unresponsive or we're just done working in it, what key do we add to the combo to 'kill' the pane?

Answer: {% spoiler x %}

**#22**

> Now that's we've finished out work, what can we type to close the session? 

Answer: {% spoiler exit %}

**#23**

> Last but now least, how do we spawn a named tmux session named 'neat'?

Answer: {% spoiler tmux new -s neat %}
