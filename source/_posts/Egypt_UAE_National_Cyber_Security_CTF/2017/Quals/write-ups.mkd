---
layout: post
title: "Egypt & UAE National Cyber Security CTF Quals 2017 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - forensics
  - crypto
  - web
date: 2017/04/10
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By    | Version | Comment
| ---   | ---     | ---
| noraj | 1.0     | Creation

### CTF

- **Name** : Egypt & UAE National Cyber Security CTF Quals 2017
- **Website** : [cybertalents.com](http://www.cybertalents.com/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/445/)

## 25 - G&P List - Digital Forensics

> Just Open the File and Capture the flag . Submission in MD5
>
> Challenge Link: https://s3-eu-west-1.amazonaws.com/talentchallenges/Forensics/G%26P+lists.docx

```
$ strings G\&P+lists.docx | grep -i flag                                                                               noraj@noraj-pc
Flag.txt877c1fa0445adaedc5365d9c139c5219PK
Flag.txt
```

You can see the flag right here between `Flag.txt` and `PK`. But you can extract the `dock` as an archive and just read the `Flag.txt` content.

## 25 - Hidden Message - Digital Forensics

> A cyber Criminal is hiding information in the below file . capture the flag ? submit Flag in MD5 Format
>
> Challenge Link: https://s3-eu-west-1.amazonaws.com/talentchallenges/Forensics/hidden_message.jpg

Let's see exif metadata:

```
$ exiftool hidden_message.jpg | grep -i Notice                                                                     noraj@noraj-pc
Copyright Notice                : b1a1f2855d2428930e0c9c4ce10500d5
```

## 25 - Crack the Hash - Cryptography

> A hacker leaked the below hash online.Can you crack it to know the password of the CEO? 1ab566b9fa5c0297295743e7c2a6ec27

```
$ hashid 1ab566b9fa5c0297295743e7c2a6ec27
Analyzing '1ab566b9fa5c0297295743e7c2a6ec27'
[+] MD2
[+] MD5
[+] MD4
[+] Double MD5
[+] LM
[+] RIPEMD-128
[+] Haval-128
[+] Tiger-128
[+] Skein-256(128)
[+] Skein-512(128)
[+] Lotus Notes/Domino 5
[+] Skype
[+] Snefru-128
[+] NTLM
[+] Domain Cached Credentials
[+] Domain Cached Credentials 2
[+] DNSSEC(NSEC3)
[+] RAdmin v2.x
```

It must be a MD5 hash. I used the `findmyhash` script but had no result. Then I tried online on [hashkiller](https://hashkiller.co.uk/md5-decrypter.aspx).

The flag was `Iamtheflag`.

## 50 - Admin has the power - Web Security

If we take a look at the source code we can see:

```html
<!-- TODO: remove this line ,  for maintenance purpose use this info (user:support password:x34245323)-->
```

And if we login as the `support` user we get the following message:

```
Hi support
Your privilege is support , may be you need better privilages !!
```

Our cookie look likes this: `PHPSESSID=uq82s6g3kdbknicsj8eb040pj0; role=support`. Maybe we can change the `role`.

We just used burp as proxy to intercept our request and change our cookie to `PHPSESSID=uq82s6g3kdbknicsj8eb040pj0; role=admin`.

The result is:

```
Hi admin
Admin Secret flag : hiadminyouhavethepower
```

## 50 - Cypher Anxiety - Digital Forensics

**Warning**: We get the picture but we didn't get the right flag.

```
$ strings find\ the\ image.pcap| head -11
Hey bro
S\U
Sup supp, are we ready
yeah, u got the files?
yes but i think the channel is not secured
the UTM will block the file transfer as the DLP module is active
ok we can use cryptcat
ok what the password then
let it be P@ssawordaya
hhh, ok
listen on 7070 and ill send you the file , bye
```

So they use cryptcat over the port 7070 with the secret key `P@ssawordaya`.

With wireshark let's filter `tcp.port == 7070`, then flow TCP stream and save it as raw file.

Then open a netcat client on localhost: `netcat localhost 7070 < crypted.file`.

And open a cryptcat listener on localhost: `cryptcat -l -k P@ssawordaya -p 7070  > decrypted.file`.


```
$ file decrypted.jpeg              noraj@noraj-pc
decrypted.jpeg: JPEG image data, JFIF standard 1.02, resolution (DPI), density 72x72, segment length 16, Exif Standard: [TIFF image data, big-endian, direntries=7, orientation=upper-left, xresolution=98, yresolution=106, resolutionunit=2, software=Adobe Photoshop 7.0, datetime=2012:07:30 17:31:00]

$ binwalk decrypted.jpeg           noraj@noraj-pc

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.02
30            0x1E            TIFF image data, big-endian, offset of first image directory: 8
324           0x144           JPEG image data, JFIF standard 1.02
4279          0x10B7          JPEG image data, JFIF standard 1.02
7186          0x1C12          Unix path: /www.w3.org/1999/02/22-rdf-syntax-ns#' xmlns:iX='http://ns.adobe.com/iX/1.0/'>
7355          0x1CBB          Unix path: /ns.adobe.com/xap/1.0/mm/'>

$ exiftool decrypted.jpeg| grep -i "thumbnail image"
Thumbnail Image                 : (Binary data 2561 bytes, use -b option to extract)

$ exiftool -b -ThumbnailImage decrypted.jpeg > thumb.jpeg
```

Both submiting md5 hash of the picture or the thumbnail failed. We didn't get the flag.
