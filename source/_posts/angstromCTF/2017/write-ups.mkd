---
layout: post
title: "angstromCTF 2017 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - crypto
  - forensics
date: 2017/04/29
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By    | Version | Comment
| ---   | ---     | ---
| noraj | 1.0     | Creation

### CTF

- **Name** : angstromCTF 2017
- **Website** : [angstromctf.com](Xhttps://www.angstromctf.com/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/435)

## 10 - The Beginning - Crypto

> Pxevhfx mh tgzlmkhfvmy. Px ahix rhn xgchr hnk vmy. tvmy{utvd_mh_max_ynmnkx}.

ROT 7:

`Welcome to angstromctf. We hope you enjoy our ctf. actf{back_to_the_future}.`

## 50 - Image Trickery - Forensics

> What do Twitch emotes have to do with a CTF? No idea, but there's a flag in [here](https://angstromctf.com/static/forensics/image_trickery/mystery.png) somewhere. *kappa*

With Stegsolve we can see a QR code un blue plane 0 or grey bit plane:

![blue plane 0](https://i.imgur.com/HDgXICa.png) ![grey bit plane](https://i.imgur.com/1WHXcwh.png)

Using WebQR to solve the QR code we found a pastebin link: https://pastebin.com/S9De6WYA (I made a backup [link](https://ghostbin.com/paste/yj2zy)).

The content is a base64 string but I'm not pasting it here as it is very long.

So let's decode it:

```
$ cat base64.txt| base64 -di > test

$ xxd -l32 test
00000000: 6461 7461 3a69 6d61 6765 2f73 7667 2b78  data:image/svg+x
00000010: 6d6c 3b62 6173 6536 342c 5044 3934 6257  ml;base64,PD94bW
```

Now it looks like a SVG image in base64. So I kept only the base64 image and removed `data:image/svg+xml;base64,`. Then I extracted the image:

```
$ cat test | base64 -di > image.svg
```

Display it and read: `actf{fa1L_F15H}`, we got the flag.

**Note**: again and ever, this is not Forensics but Steganography.

## 60 - Document - Forensics

> Defund wrecked his [essay](https://angstromctf.com/static/forensics/document/essay.docx) in a text editor to pretend that his file got corrupted.

Word's DOCX (Office Open XML) is just a zip containing some XML files.

Let's try to (partially) fix the archive:

```
$ zip -FF ./essay.docx --out fixed.zip
Fix archive (-FF) - salvage what can
	zip warning: Missing end (EOCDR) signature - either this archive
                     is not readable or the end is damaged
Is this a single-disk archive?  (y/n): y
  Assuming single-disk archive
Scanning for entries...
 copying: word/settings.xml  (936 bytes)
 copying: word/fontTable.xml  (442 bytes)
 copying: word/webSettings.xml  (265 bytes)
 copying: docProps/app.xml  (308 bytes)
 copying: docProps/core.xml  (332 bytes)
 copying: word/styles.xml  (2799 bytes)
 copying: word/document2.xml  (2432 bytes)
 copying: word/_rels/document2.xml.rels  (232 bytes)
```

Now unzip it and look at `word/document2.xml`:

```
cat word/document2.xml| grep -i actf
```

We can see `actf{too_bad_for_zip_recovery`, just add the ending curly brace and you have the flag: `actf{too_bad_for_zip_recovery}`.
