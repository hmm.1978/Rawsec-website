---
layout: post
title: "Setup a NTP client on ArchLinux"
date: 2020/10/10 20:40:00
lang: en
categories:
- linux
- archlinux
tags:
- linux
- archlinux
thumbnail: /images/archlinux.svg
authorId: noraj
toc: true
---
There are at least 4 NTP clients available on ArchLinux:

- [systemd-timesyncd](https://wiki.archlinux.org/index.php/Systemd-timesyncd)
- [ntpd](https://wiki.archlinux.org/index.php/Network_Time_Protocol_daemon)
- [chrony](https://wiki.archlinux.org/index.php/Chrony)
- [openntpd](https://wiki.archlinux.org/index.php/OpenNTPD)

We'll see how to configure **systemd-timesyncd**, the most handy of the four.

## Configuration

Create a directory `/etc/systemd/timesyncd.conf.d/` to override the default `/etc/systemd/timesyncd.conf`:

```
$ sudo mkdir /etc/systemd/timesyncd.conf.d/
```

Create our custom configuration file:

```
$ sudoedit /etc/systemd/timesyncd.conf.d/servers.conf
```

Optionally RTFM about the config format:

```
$ man 5 timesyncd.conf
```

Add the ntp servers:

```ini
[Time]
NTP=0.fr.pool.ntp.org 1.fr.pool.ntp.org 2.fr.pool.ntp.org 3.fr.pool.ntp.org
FallbackNTP=0.arch.pool.ntp.org 1.arch.pool.ntp.org 2.arch.pool.ntp.org 3.arch.pool.ntp.org
```

Restart the time sync service to take the new config into effect:

```
$ sudo systemctl restart systemd-timesyncd.service
```

To verify your configuration was taken into account:

```
$ timedatectl show-timesync --all
LinkNTPServers=
SystemNTPServers=0.fr.pool.ntp.org 1.fr.pool.ntp.org 2.fr.pool.ntp.org 3.fr.pool.ntp.org
FallbackNTPServers=0.arch.pool.ntp.org 1.arch.pool.ntp.org 2.arch.pool.ntp.org 3.arch.pool.ntp.org
ServerName=0.fr.pool.ntp.org
ServerAddress=37.59.58.17
RootDistanceMaxUSec=5s
PollIntervalMinUSec=32s
PollIntervalMaxUSec=34min 8s
PollIntervalUSec=1min 4s
NTPMessage={ Leap=0, Version=4, Mode=4, Stratum=2, Precision=-21, RootDelay=12.069ms, RootDispersion=28.411ms, Reference=AAE7FA9E, OriginateTimestamp=Wed 2020-04-01 12:18:22 CEST, ReceiveTimestamp=Wed 2020-04-01 12:18:22 CEST, TransmitTimestamp=Wed 2020-04-01 12:18:22 CEST, DestinationTimestamp=Wed 2020-04-01 12:18:22 CEST, Ignored=no PacketCount=1, Jitter=0 }
Frequency=2190623
```

## Usage

To enable and start the service:

```
$ sudo timedatectl set-ntp true
```

At anytime you can check if ntp is active the time, etc.

```
$ timedatectl status
               Local time: mer. 2020-04-01 12:21:08 CEST
           Universal time: mer. 2020-04-01 10:21:08 UTC
                 RTC time: mer. 2020-04-01 10:21:08
                Time zone: Europe/Paris (CEST, +0200)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
```

You can also have more verbose information about ntp:

```
$ timedatectl timesync-status
       Server: 37.59.58.17 (0.fr.pool.ntp.org)
Poll interval: 4min 16s (min: 32s; max 34min 8s)
         Leap: normal
      Version: 4
      Stratum: 2
    Reference: AAE7FA9E
    Precision: 1us (-21)
Root distance: 37.345ms (max: 5s)
       Offset: +82us
        Delay: 16.948ms
       Jitter: 301us
 Packet count: 3
    Frequency: +36,922ppm
```
