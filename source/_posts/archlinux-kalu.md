---
layout: post
title: "Kalu - Keeping ArchLinux Up-to-date"
date: 2020/05/23
lang: en
categories:
- linux
- archlinux
tags:
- linux
- archlinux
thumbnail: /images/archlinux.svg
authorId: noraj
toc: true
---
## Intro

Basically updating ArchLinux is as easy as running `pacman -Syu`. But blindly
running `pacman -Syu` everyday is not necessarily a good idea. Sometimes
some updates require manual intervention, so you have to be aware of it
**before** you launch the update.

## Level 1 - ArchLinux news website

The basic solution is to check news on the ArchLinux homepage
(https://www.archlinux.org/)

![](https://i.imgur.com/buQI3Gw.png)

or to check the ArchLinux News page (https://www.archlinux.org/news/)

![](https://i.imgur.com/ocMHNVm.png)

before each update.

But doing this manually is long and boring right?

Even using a RSS reader you have to check your RSS app.

## Level 2 - Pacman wrappers

One category of [AUR helpers](https://wiki.archlinux.org/index.php/AUR_helpers)
are _Pacman wrappers_.
Some of pacman wrappers like [pikaur](https://github.com/actionless/pikaur)
shows unread Arch news before sysupgrade, so when doing `pikaur -Syu` you
will be able to read the latest news before upgrading.

But not all pacman wrappers have a news system like _pikaur_ and I personally
prefer to keep the use of pacman wrapper to a minimum. In my case I use
_pikaur_ only for installing and updating AUR packages and use _pacman_ for
ArchLinux repository packages.

After all the ArchLinux wiki warns you:

> Warning: pacman(8) wrappers abstract the work of the package manager. They may (optionally or by default) introduce unsafe flags, or other unexpected behavior leading to a defective system.

## Level 3 - Kalu

Let's see **what is [Kalu](https://jjacky.com/kalu/)**:

> kalu (which could stand for "Keeping Arch Linux Up-to-date") is a small application that will add an icon to your systray and sit there, regularly checking if there's anything new for you to upgrade. As soon as it finds something, it'll show a notification to let you know about it.

But Kalu not only tells you when there is new updates, it also notify you of
unread ArchLinux news and AUR package updates. But we'll come back to it.

Some strong points:

- unprivileged (doesn't require root)
- partial upgrade free (it doesn't synchronize databases, it creates a temporary copy of your sync databases, sync those, and then remove them)
- designed to avoid dangerous situation

**What does it check for?**

- Arch Linux News
- Available upgrades for installed packages
- Available upgrades for non-installed packages (you can define a list of watched packages)
- Available upgrades for AUR packages
- Available upgrades for watched AUR packages (you can define a list of watched packages)

**When does it check?**

I kept the check every hour (default) frequency but you can choose any other
and even define some _paused period_.

**Notifications**

As here our goal is to be aware of ArchLinux News, the point that interest us
the most is that Kalu will display notifications each time it does a check.

You can also click on the Kalu icon on the systray to make it do a manual check.

![](https://i.imgur.com/ERGDS5H.png)

So it notify me when there is a new update and at the same time I know if there
is a new ArchLinux news or not and so if I can safely make the update or not.

Here a example of notification for available updates (KDE/Plasma):

![](https://i.imgur.com/s8Tz5LU.png)

You have a similar notification for the unread news.

On the contextual menu

![](https://i.imgur.com/IozL4PH.png)

you can ask to display recent ArchLinux news.

![](https://i.imgur.com/2syJupV.png)

So you will be able to re-read news even if you inadvertently checked the latest
news as read.

**More**

Kalu does a lot more than just that but it's not the point here. To dig deeper
check the [official page](https://jjacky.com/kalu/).

**AUR packages**

- [kalu-kde](https://aur.archlinux.org/packages/kalu-kde/) (KDE/plasma/Qt supports)
- [kalu](https://aur.archlinux.org/packages/kalu/) (GTK supports)
- [kalu-cli](https://aur.archlinux.org/packages/kalu-cli/) (CLI only, no GUI)
